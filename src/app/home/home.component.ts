import { Condition } from './../shared/condition.model';
import { Case } from './../shared/case.mode';
import { Component, OnInit } from '@angular/core';
import { CasesService } from '../shared/cases.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  public cases: Case[];
  public condition: Condition = null;
  public carForm: FormGroup;
  public timer: any;

  constructor(
    private _casesService: CasesService,
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this._casesService.getCases(1001).subscribe(res => {
      let data: any = res;
      this.cases = JSON.parse(data._body).results.reverse();
    });
    this.buildForm();
  }

  buildForm() {
    this.carForm = this._fb.group({
      car_id: [null, Validators.required]
    });
  }

  getCondition() {
    this._casesService.getCondition(this.carForm.value.car_id).subscribe(res => {
      this.carForm.reset();
      let data: any = res;
      let i=1;
      data = JSON.parse(data._body).condition;
      this.condition = data[0];      
      setInterval(() => {
        this.condition = data[i];
        i++;
      }, 3000);
    });
  }

}
