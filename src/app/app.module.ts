import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CreateCaseComponent } from './create-case/create-case.component';
import { CasesService } from '../app/shared/cases.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateCaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CasesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
