export class Case {
  public car_id: string;
  public case_no: string;
  public case_type: string;
  public title: string;
  public desc: string;
  public expected: string;
  public priority: string;
  public service_rep: string;
  public service_rep_contact: string;
  public status: string;

  constructor(data: any) {
    this.car_id = data.car_id,
    this.case_no = data.case_no,
    this.case_type = data.case_type,
    this.title = data.title,
    this.desc = data.desc,
    this.expected = data.expected,
    this.priority = data.priority,
    this.service_rep = data.service_rep,
    this.service_rep_contact = data.service_rep_contact,
    this.status = data.status
  }
}