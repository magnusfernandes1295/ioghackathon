import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CasesService {

  constructor(
    private _http: Http
  ) { }

  getCases(id: number) {
    return this._http.get('http://localhost:3000/getcase/' + id);
  }

  getCondition(id: number) {
    return this._http.get('http://localhost:3000/getcondition/' + id);
  }
}
