export class Condition {
  public batt: string;
  public duedate: string;
  public ect: string;
  public map: string;
  public eng_load: string;
  public ltft: string;
  public rpm: string;
  public stft: string;

  constructor(data: any) {
    this.batt = data.batt,
    this.duedate = data.duedate,
    this.ect = data.ect,
    this.map = data.map,
    this.eng_load = data.eng_load,
    this.ltft = data.ltft,
    this.rpm = data.rpm,
    this.stft = data.stft
  }
}